# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [GNU PSPP])
m4_define([AT_PACKAGE_TARNAME],   [pspp])
m4_define([AT_PACKAGE_VERSION],   [2.0.1])
m4_define([AT_PACKAGE_STRING],    [GNU PSPP 2.0.1])
m4_define([AT_PACKAGE_BUGREPORT], [bug-gnu-pspp@gnu.org])
m4_define([AT_PACKAGE_URL],       [https://www.gnu.org/software/pspp/])
